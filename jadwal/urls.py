from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'jadwal'

urlpatterns = [
	path('schedule/',views.CalendarView.as_view(), name='jadwal'),
	url(r'^event/new/$', views.event, name='event_new'),
    url(r'^event/edit/(?P<event_id>\d+)/$', views.event, name='event_edit'),
]