from django.forms import ModelForm, DateInput, TimeInput
from jadwal.models import Event

class EventForm(ModelForm):
  class Meta:
    model = Event
    widgets = {
        'day' : DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
        'start_time': TimeInput(attrs={'type': 'time'}),
        'end_time': TimeInput(attrs={'type': 'time'}),
    }
    fields = '__all__'

  def __init__(self, *args, **kwargs):
    super(EventForm, self).__init__(*args, **kwargs)
    self.fields['day'].input_formats = ('%Y-%m-%d',)
    self.fields['start_time'].input_formats = ('%H:%M',)
    self.fields['end_time'].input_formats = ('%H:%M',)