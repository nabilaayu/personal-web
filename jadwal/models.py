from django.db import models
from django.urls import reverse
from django.core.exceptions import ValidationError
from datetime import date, datetime
from django.utils import timezone

class Event(models.Model):
    event= models.CharField(max_length=255)
    category= models.CharField(max_length=255)
    place= models.CharField(max_length=255)
    day = models.DateField(u'Day of the event')
    start_time = models.TimeField(u'Starting time', help_text=u'hh:mm')
    end_time = models.TimeField(u'Final time', help_text=u'hh:mm')

    @property
    def get_html_url(self):
        url = reverse('jadwal:event_edit', args=(self.id,))
        return f'<a href="{url}"> {self.event} </a>'

    def clean(self):
        if self.end_time <= self.start_time:
            raise ValidationError('Ending times must be after starting times')
 
        events = Event.objects.filter(day=self.day)
        if events.exists():
            for event in events:
                if self.check_overlap(event.start_time, event.end_time, self.start_time, self.end_time):
                    raise ValidationError('There is an overlap with another event: ' + str(event.day) + ', ' + str(event.start_time) + '-' + str(event.end_time))
    
    def check_overlap(self, fixed_start, fixed_end, new_start, new_end):
        overlap = False
        if new_start == fixed_end or new_end == fixed_start:    #edge case
            overlap = False
        elif (new_start >= fixed_start and new_start <= fixed_end) or (new_end >= fixed_start and new_end <= fixed_end): #innner limits
            overlap = True
        elif new_start <= fixed_start and new_end >= fixed_end: #outer limits
            overlap = True

        return overlap
