from django.shortcuts import render,redirect

# Create your views here.
def index(request):
    return render(request,'pages/index.html',{})
def about(request):
    return render(request,'pages/about.html',{})
def email(request):
    return render(request,'pages/email.html',{})
def portfolio(request):
    return render(request,'pages/portfolio.html',{})
def temanbelajar(request):
    return render(request,'pages/temanbelajar.html',{})
def jadwal(request):
    return render(request,'jadwal/jadwal.html',{})