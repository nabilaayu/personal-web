from django.urls import path
from django.conf.urls import url
from . import views
from jadwal.views import CalendarView, event

app_name = 'tugas'

urlpatterns = [
	path('',views.index,name='index'),
	path('portfolio/',views.portfolio,name='portfolio'),
	path('about/',views.about,name='about'),
	path('email/',views.email,name='email'),
	path('temanbelajar/',views.temanbelajar,name='temanbelajar'),
]